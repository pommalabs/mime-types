﻿#load "nuget: PommaLabs.Salatino.BuildTargets, 1.93.0"

#r "nuget: DiffPlex, 1.6.3"

using System.IO;
using static Bullseye.Targets;
using static SimpleExec.Command;

////////////////////////////////////////////////////////////////////////////////
// PREPARATION
////////////////////////////////////////////////////////////////////////////////

var srcMapFile = "./src/PommaLabs.MimeTypes/MimeTypeMap.Generated.cs";
var testsMapFile = "./tests/PommaLabs.MimeTypes.UnitTests/MimeTypeMapTests.Generated.cs";
var tmpDir = Path.GetTempPath();
var srcMapCopyFile = $"{tmpDir}/MimeTypeMap.cs.bak";
var testsMapCopyFile = $"{tmpDir}/MimeTypeMapTests.cs.bak";

////////////////////////////////////////////////////////////////////////////////
// TARGETS
////////////////////////////////////////////////////////////////////////////////

Target("update-maps", async () =>
{
    var generatorDir = "./build/PommaLabs.MimeTypes.Generator/";
    var (srcMap, _) = await ReadAsync("dotnet", $"run -- src", generatorDir);
    var (testsMap, _) = await ReadAsync("dotnet", $"run -- tests", generatorDir);
    var utf8WithBom = new UTF8Encoding(encoderShouldEmitUTF8Identifier: true);

    await File.WriteAllTextAsync(srcMapFile, srcMap, utf8WithBom);
    await File.WriteAllTextAsync(testsMapFile, testsMap, utf8WithBom);
});

Target("copy-maps", () =>
{
    File.Copy(srcMapFile, srcMapCopyFile, overwrite: true);
    File.Copy(testsMapFile, testsMapCopyFile, overwrite: true);
});

Target("check-new-mime-types", DependsOn("copy-maps", "update-maps"), async () =>
{
    var srcMap = await File.ReadAllTextAsync(srcMapFile);
    var testsMap = await File.ReadAllTextAsync(testsMapFile);
    var srcMapCopy = await File.ReadAllTextAsync(srcMapCopyFile);
    var testsMapCopy = await File.ReadAllTextAsync(testsMapCopyFile);

    var srcMapDiffs = DiffPlex.DiffBuilder.InlineDiffBuilder.Diff(srcMap, srcMapCopy);
    var testsMapDiffs = DiffPlex.DiffBuilder.InlineDiffBuilder.Diff(testsMap, testsMapCopy);

    if (srcMapDiffs.HasDifferences || testsMapDiffs.HasDifferences)
    {
        File.Copy(srcMapCopyFile, srcMapFile, overwrite: true);
        File.Copy(testsMapCopyFile, testsMapFile, overwrite: true);
        throw new Exception("Source MIME types have been changed");
    }
});

////////////////////////////////////////////////////////////////////////////////
// EXECUTION
////////////////////////////////////////////////////////////////////////////////

await Build(Args);
