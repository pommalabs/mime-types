﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Collections.Generic;

namespace PommaLabs.MimeTypes;

/// <summary>
///   Data related to a specific MIME type.
/// </summary>
internal readonly struct MimeData
{
    /// <summary>
    ///   Constructor.
    /// </summary>
    /// <param name="encoding">Encoding.</param>
    /// <param name="extensions">Extensions.</param>
    public MimeData(MimeEncoding encoding, IReadOnlyList<string> extensions)
    {
        Encoding = encoding;
        Extensions = extensions;
    }

    /// <summary>
    ///   Encoding.
    /// </summary>
    public MimeEncoding Encoding { get; }

    /// <summary>
    ///   Extensions (with leading dot).
    /// </summary>
    public IReadOnlyList<string> Extensions { get; }
}
