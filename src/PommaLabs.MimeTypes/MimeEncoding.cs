﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System.Diagnostics.CodeAnalysis;

namespace PommaLabs.MimeTypes;

/// <summary>
///   MIME transfer encoding values.
/// </summary>
public enum MimeEncoding
{
    /// <summary>
    ///   "base64" encoding.
    /// </summary>
    Base64 = 0,

    /// <summary>
    ///   "7bit" encoding.
    /// </summary>
    [SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "Identifiers cannot have leading numbers")]
    _7bit = 1,

    /// <summary>
    ///   "8bit" encoding.
    /// </summary>
    [SuppressMessage("Naming", "CA1707:Identifiers should not contain underscores", Justification = "Identifiers cannot have leading numbers")]
    _8bit = 2,

    /// <summary>
    ///   "quoted-printable" encoding.
    /// </summary>
    QuotedPrintable = 3,
}
