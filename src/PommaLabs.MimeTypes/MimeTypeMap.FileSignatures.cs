﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Linq;

namespace PommaLabs.MimeTypes;

public static partial class MimeTypeMap
{
    /// <summary>
    ///   <para>
    ///     Source data:
    ///     - https://www.garykessler.net/library/file_sigs.html
    ///     - https://en.wikipedia.org/wiki/List_of_file_signatures
    ///     - https://asecuritysite.com/forensics/magic
    ///     - https://github.com/apache/tika/blob/main/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml
    ///   </para>
    ///   <para>Comments are copied from source data, in order to have a quick reference.</para>
    /// </summary>
    private static readonly Lazy<List<FileSignature>> s_fileSignatures = new(() =>
    [
        .. new List<FileSignature>
        {
            new()
            {
                // 3rd Generation Partnership Project 3GPP multimedia files.
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x33, 0x67, 0x70],
                MimeTypes = _ => s_extensionMap.Value[Extensions._3GP].Union(s_extensionMap.Value[Extensions._3G2]).ToList(),
            },
            new()
            {
                // 7-Zip File Format.
                Signature = [0x37, 0x7A, 0xBC, 0xAF, 0x27, 0x1C],
                MimeTypes = _ => s_extensionMap.Value[Extensions._7Z],
            },
            new()
            {
                // Adaptive Multi-Rate ACELP (Algebraic Code Excited Linear Prediction)
                // Codec, commonly audio format with GSM cell phones. (See RFC 4867.)
                Signature = [0x23, 0x21, 0x41, 0x4D, 0x52],
                MimeTypes = _ => s_extensionMap.Value[Extensions.AMR],
            },
            new()
            {
                // Resource Interchange File Format - Windows Audio Video Interleave file.
                Signature =
                [
                    0x52, 0x49, 0x46, 0x46, ByteRange.Any, ByteRange.Any, ByteRange.Any, ByteRange.Any, 0x41, 0x56, 0x49,
                    0x20, 0x4C, 0x49, 0x53, 0x54
                ],
                MimeTypes = _ => s_extensionMap.Value[Extensions.AVI],
            },
            new()
            {
                // Alliance for Open Media (AOMedia) Video 1 (AV1) Image File - ftypavif.
                Signature = [0x00, 0x00, 0x00, ByteRange.Any, 0x66, 0x74, 0x79, 0x70, 0x61, 0x76, 0x69, 0x66],
                MimeTypes = _ => s_extensionMap.Value[Extensions.AVIF],
            },
            new()
            {
                // Windows (or device-independent) bitmap image.
                Signature = [0x42, 0x4D],
                MimeTypes = _ => s_extensionMap.Value[Extensions.BMP],
            },
            new()
            {
                // MS Windows HtmlHelp Data.
                Signature = [0x49, 0x54, 0x53, 0x46, 0x03, 0x00, 0x00, 0x00, 0x60, 0x00, 0x00, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.CHM],
            },
            new()
            {
                // Canon digital camera RAW file.
                Signature = [0x49, 0x49, 0x2A, 0x00, 0x10, 0x00, 0x00, 0x00, 0x43, 0x52],
                MimeTypes = _ => s_extensionMap.Value[Extensions.CR2],
            },
            new()
            {
                // Canon digital camera RAW file.
                Signature = [0x49, 0x49, 0x1A, 0x00, 0x00, 0x00, 0x48, 0x45, 0x41, 0x50, 0x43, 0x43, 0x44, 0x52, 0x02, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.CRW],
            },
            new()
            {
                // Canon digital camera RAW file.
                Signature = [0xC2, 0x07],
                MimeTypes = _ => s_extensionMap.Value[Extensions.CRW],
            },
            new()
            {
                // DICOM Medical File Format.
                MinOffset = 128,
                Signature = [0x44, 0x49, 0x43, 0x4D],
                MimeTypes = _ => s_extensionMap.Value[Extensions.DCM],
            },
            new()
            {
                // Adobe encapsulated PostScript file.
                Signature = [0x25, 0x21, 0x50, 0x53],
                MimeTypes = _ => s_extensionMap.Value[Extensions.EPS],
            },
            new()
            {
                // Flash video file.
                Signature = [0x46, 0x4C, 0x56, 0x01],
                MimeTypes = _ => s_extensionMap.Value[Extensions.FLV],
            },
            new()
            {
                // Graphics interchange format file.
                Signature = [0x47, 0x49, 0x46, 0x38, 0x37, 0x61],
                MimeTypes = _ => s_extensionMap.Value[Extensions.GIF],
            },
            new()
            {
                // Graphics interchange format file.
                Signature = [0x47, 0x49, 0x46, 0x38, 0x39, 0x61],
                MimeTypes = _ => s_extensionMap.Value[Extensions.GIF],
            },
            new()
            {
                // High Efficiency Image Container (HEIC) - ftypheic.
                Signature = [0x00, 0x00, 0x00, ByteRange.Any, 0x66, 0x74, 0x79, 0x70, 0x68, 0x65, 0x69, 0x63],
                MimeTypes = _ => s_extensionMap.Value[Extensions.HEIC],
            },
            new()
            {
                // High Efficiency Image Container (HEIC) - ftypmif1.
                Signature = [0x00, 0x00, 0x00, ByteRange.Any, 0x66, 0x74, 0x79, 0x70, 0x6D, 0x69, 0x66, 0x31],
                MimeTypes = _ => s_extensionMap.Value[Extensions.HEIC],
            },
            new()
            {
                // Computer icon encoded in ICO file format.
                Signature = [0x00, 0x00, 0x01, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.ICO],
            },
            new()
            {
                // Generic JPEG image file.
                Signature = [0xFF, 0xD8],
                MimeTypes = _ => s_extensionMap.Value[Extensions.JPEG],
            },
            new()
            {
                // Image encoded in the JPEG XL format.
                Signature = [0xFF, 0x0A],
                MimeTypes = _ => s_extensionMap.Value[Extensions.JXL],
            },
            new()
            {
                // Image encoded in the JPEG XL format.
                Signature = [0x00, 0x00, 0x00, 0x0C, 0x4A, 0x58, 0x4C, 0x20, 0x0D, 0x0A, 0x87, 0x0A],
                MimeTypes = _ => s_extensionMap.Value[Extensions.JXL],
            },
            new()
            {
                // Matroska stream file.
                Signature = [0x1A, 0x45, 0xDF, 0xA3],
                MimeTypes = _ => s_extensionMap.Value[Extensions.MKV].Union(s_extensionMap.Value[Extensions.WEBM]).ToList(),
            },
            new()
            {
                // MP3 file with an ID3v2 container.
                Signature = [0x49, 0x44, 0x33],
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP3],
            },
            new()
            {
                // MPEG-1 Layer 3 file without an ID3 tag or with an ID3v1 tag (which is appended at
                // the end of the file).
                Signature = [0xFF, new ByteRange(0xE0, 0xEF)],
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP3],
            },
            new()
            {
                // MPEG-1 Layer 3 file without an ID3 tag or with an ID3v1 tag (which is appended at
                // the end of the file).
                Signature = [0xFF, new ByteRange(0xF0, 0xFF)],
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP3],
            },
            new()
            {
                // ISO Media, MPEG v4 system or iTunes AVC-LC file.
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x4D, 0x34, 0x56, 0x20], // "ftypM4V "
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // https://github.com/apache/tika/blob/33c8b7bcb0e00f097076d837f77428b496a3f22d/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml#L8512
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x4D, 0x34, 0x56, 0x48], // "ftypM4VH"
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // https://github.com/apache/tika/blob/33c8b7bcb0e00f097076d837f77428b496a3f22d/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml#L8513
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x4D, 0x34, 0x56, 0x50], // "ftypM4VP"
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // MPEG-4 video or QuickTime file.
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x6D, 0x70, 0x34, 0x31], // "ftypmp41"
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // MPEG-4 video or QuickTime file.
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x6D, 0x70, 0x34, 0x32], // "ftypmp42"
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // https://github.com/apache/tika/blob/33c8b7bcb0e00f097076d837f77428b496a3f22d/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml#L5637
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x4D, 0x34, 0x41, 0x20], // "ftypM4A "
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // https://github.com/apache/tika/blob/33c8b7bcb0e00f097076d837f77428b496a3f22d/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml#L5637
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x4D, 0x34, 0x41, 0x00], // "ftypM4A "
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // https://github.com/apache/tika/blob/33c8b7bcb0e00f097076d837f77428b496a3f22d/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml#L5638C21-L5638C28
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x4D, 0x34, 0x42, 0x20], // "ftypM4B "
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // https://github.com/apache/tika/blob/33c8b7bcb0e00f097076d837f77428b496a3f22d/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml#L5638C21-L5638C28
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x46, 0x34, 0x41, 0x20], // "ftypF4A "
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // https://github.com/apache/tika/blob/33c8b7bcb0e00f097076d837f77428b496a3f22d/tika-core/src/main/resources/org/apache/tika/mime/tika-mimetypes.xml#L5638C21-L5638C28
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x46, 0x34, 0x42, 0x20], // "ftypF4B "
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // MPEG-4 video file.
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x4D, 0x53, 0x4E, 0x56], // "ftypMSNV"
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // ISO Base Media file (MPEG-4) v1.
                MinOffset = 4,
                Signature = [0x66, 0x74, 0x79, 0x70, 0x69, 0x73, 0x6F, 0x6D], // "ftypisom"
                MimeTypes = _ => s_extensionMap.Value[Extensions.MP4],
            },
            new()
            {
                // Ogg Vorbis Codec compressed Multimedia file.
                Signature = [0x4F, 0x67, 0x67, 0x53, 0x00, 0x02, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.OGG],
            },
            new()
            {
                // Adobe Portable Document Format, Forms Document Format, and Illustrator graphics files.
                // Header format is "%PDF−M.m", where M/m are numbers.
                // Signature must be specified within first 1024 bytes.
                // See also: https://opensource.adobe.com/dc-acrobat-sdk-docs/pdfstandards/pdfreference1.3.pdf
                MaxOffset = 1023 - 8 /* Signature length */,
                Signature = [0x25, 0x50, 0x44, 0x46, 0x2D, ByteRange.Number, 0x2E, ByteRange.Number],
                MimeTypes = _ => s_extensionMap.Value[Extensions.PDF],
            },
            new()
            {
                // Adobe Portable Document Format, Forms Document Format, and Illustrator graphics files.
                // Header format is "%!PS−Adobe−N.n PDF−M.m", where N/n/M/m are numbers.
                // Signature must be specified within first 1024 bytes.
                // See also: https://opensource.adobe.com/dc-acrobat-sdk-docs/pdfstandards/pdfreference1.3.pdf
                MaxOffset = 1023 - 22 /* Signature length */,
                Signature =
                [
                    0x25, 0x21, 0x50, 0x53, 0x2D, 0x41, 0x64, 0x6F, 0x62, 0x65, 0x2D, ByteRange.Number, 0x2E,
                    ByteRange.Number, 0x20, 0x50, 0x44, 0x46, 0x2D, ByteRange.Number, 0x2E, ByteRange.Number
                ],
                MimeTypes = _ => s_extensionMap.Value[Extensions.PDF],
            },
            new()
            {
                // Portable Network Graphics file.
                Signature = [0x89, 0x50, 0x4E, 0x47, 0x0D, 0x0A, 0x1A, 0x0A],
                MimeTypes = _ => s_extensionMap.Value[Extensions.PNG],
            },
            new()
            {
                // Photoshop image file.
                Signature = [0x38, 0x42, 0x50, 0x53],
                MimeTypes = _ => s_extensionMap.Value[Extensions.PSD],
            },
            new()
            {
                // Rich text format word processing file.
                Signature = [0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.RAR],
            },
            new()
            {
                // Rich text format word processing file.
                Signature = [0x52, 0x61, 0x72, 0x21, 0x1A, 0x07, 0x01, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.RAR],
            },
            new()
            {
                // Rich text format word processing file.
                Signature = [0x7B, 0x5C, 0x72, 0x74, 0x66],
                MimeTypes = _ => s_extensionMap.Value[Extensions.RTF],
            },
            new()
            {
                // Tagged Image File Format file.
                Signature = [0x49, 0x20, 0x49],
                MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF],
            },
            new()
            {
                // Tagged Image File Format file (little endian, i.e., LSB first in the byte; Intel).
                Signature = [0x49, 0x49, 0x2A, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF],
            },
            new()
            {
                // Tagged Image File Format file (big endian, i.e., LSB last in the byte; Motorola).
                Signature = [0x4D, 0x4D, 0x00, 0x2A],
                MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF],
            },
            new()
            {
                // BigTIFF files; Tagged Image File Format files >4 GB.
                Signature = [0x4D, 0x4D, 0x00, 0x2B],
                MimeTypes = _ => s_extensionMap.Value[Extensions.TIFF],
            },
            new()
            {
                // TrueType font file.
                Signature = [0x00, 0x01, 0x00, 0x00, 0x00],
                MimeTypes = _ => s_extensionMap.Value[Extensions.TTF],
            },
            new()
            {
                // vCard file with CR/LF line endings.
                Signature = [0x42, 0x45, 0x47, 0x49, 0x4E, 0x3A, 0x56, 0x43, 0x41, 0x52, 0x44, 0x0D, 0x0A],
                MimeTypes = _ => s_extensionMap.Value[Extensions.VCARD].Union(s_extensionMap.Value[Extensions.VCF]).ToList(),
            },
            new()
            {
                // vCard file with LF line ending.
                Signature = [0x42, 0x45, 0x47, 0x49, 0x4E, 0x3A, 0x56, 0x43, 0x41, 0x52, 0x44, 0x0A],
                MimeTypes = _ => s_extensionMap.Value[Extensions.VCARD].Union(s_extensionMap.Value[Extensions.VCF]).ToList(),
            },
            new()
            {
                // WebAssembly binary format.
                Signature = [0x00, 0x61, 0x73, 0x6D],
                MimeTypes = _ => s_extensionMap.Value[Extensions.WASM],
            },
            new()
            {
                // Google WebP image file, where "xx xx xx xx" is the file size.
                Signature =
                [
                    0x52, 0x49, 0x46, 0x46, ByteRange.Any, ByteRange.Any, ByteRange.Any, ByteRange.Any, 0x57, 0x45, 0x42,
                    0x50
                ],
                MimeTypes = _ => s_extensionMap.Value[Extensions.WEBP],
            },
            new()
            {
                // PKZIP archive file.
                Signature = [0x50, 0x4B, 0x03, 0x04],
                MimeTypes = InspectZipFileSignature,
            },
            new()
            {
                // PKZIP archive file (empty). Since it is empty, it is not necessary to inspect it thoroughly.
                Signature = [0x50, 0x4B, 0x05, 0x06],
                MimeTypes = _ => s_extensionMap.Value[Extensions.ZIP],
            },
            new()
            {
                // PKZIP archive file (spanned). Since it is a part of a multi-file ZIP, it is not
                // possible to inspect it thoroughly.
                Signature = [0x50, 0x4B, 0x07, 0x08],
                MimeTypes = _ => s_extensionMap.Value[Extensions.ZIP],
            },
            new()
            {
                // An Object Linking and Embedding (OLE) Compound File (CF) (i.e., OLECF) file
                // format, known as Compound Binary File format by Microsoft, used by Microsoft
                // Office 97-2003 applications (Word, PowerPoint, Excel, Wizard).
                Signature = [0xD0, 0xCF, 0x11, 0xE0, 0xA1, 0xB1, 0x1A, 0xE1],
                MimeTypes = InspectOlecfFileSignature,
            },
        }
        .OrderByDescending(x => x.Signature.Length /* Longest, more specific signatures should be evaluated first */)
    ]);
}
