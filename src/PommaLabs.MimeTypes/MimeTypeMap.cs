﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Net.Http.Headers;
using OpenMcdf;

namespace PommaLabs.MimeTypes;

/// <summary>
///   Map of MIME types, which allows two-way mapping between a file extension and a MIME type.
/// </summary>
public static partial class MimeTypeMap
{
    private const MimeEncoding DefaultEncoding = MimeEncoding.Base64;
    private const string DefaultExtension = ".bin";
    private const string DefaultMimeType = "application/octet-stream";

    /// <summary>
    ///   An empty read-only string collection, used to avoid too many allocations.
    /// </summary>
    private static readonly ReadOnlyCollection<string> s_emptyReadOnlyStringCollection = new(Array.Empty<string>());

    /// <summary>
    ///   MIME type deduced from file signatures can usually be trusted. However, "text/plain"
    ///   can be too generic because we might get better results by looking at file extension.
    ///   Same thing happens for "image/tiff" and other MIME types below.
    /// </summary>
    private static readonly HashSet<string> s_tooGenericMimeTypes =
    [
        APPLICATION.OCTET_STREAM,
        APPLICATION.X_ZIP_COMPRESSED,
        APPLICATION.ZIP,
        IMAGE.TIFF,
        TEXT.PLAIN
    ];

    /// <summary>
    ///   Adds or updates internal entries for <paramref name="mimeType"/>.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="extension">Extension.</param>
    /// <remarks>Extension must start with dot character.</remarks>
    public static void AddOrUpdate(string mimeType, string extension) => AddOrUpdate(mimeType, DefaultEncoding, new[] { extension });

    /// <summary>
    ///   Adds or updates internal entries for <paramref name="mimeType"/>.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="encoding">Encoding.</param>
    /// <param name="extension">Extension.</param>
    /// <remarks>Extension must start with dot character.</remarks>
    public static void AddOrUpdate(string mimeType, MimeEncoding encoding, string extension) => AddOrUpdate(mimeType, encoding, new[] { extension });

    /// <summary>
    ///   Adds or updates internal entries for <paramref name="mimeType"/>.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="extensions">Extensions.</param>
    /// <remarks>Extensions must start with dot character.</remarks>
    public static void AddOrUpdate(string mimeType, IEnumerable<string> extensions) => AddOrUpdate(mimeType, DefaultEncoding, extensions);

    /// <summary>
    ///   Adds or updates internal entries for <paramref name="mimeType"/>.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="encoding">Encoding.</param>
    /// <param name="extensions">Extensions.</param>
    /// <remarks>Extensions must start with dot character.</remarks>
    public static void AddOrUpdate(string mimeType, MimeEncoding encoding, IEnumerable<string> extensions)
    {
        mimeType = mimeType.ToLowerInvariant();
        var extensionList = extensions.Select(x => x.ToLowerInvariant()).ToList();

        foreach (var extension in extensionList)
        {
            if (!s_extensionMap.Value.TryGetValue(extension, out var extensionMimeTypes))
            {
                s_extensionMap.Value[extension] = extensionMimeTypes = new List<string>(1);
            }
            if (!extensionMimeTypes.Contains(mimeType))
            {
                extensionMimeTypes.Add(mimeType);
            }
        }

        if (s_mimeTypeMap.Value.TryGetValue(mimeType, out var data))
        {
            foreach (var extension in data.Extensions.Where(x => !extensionList.Contains(x)))
            {
                var extensionMimeTypes = s_extensionMap.Value[extension];
                extensionMimeTypes.Remove(mimeType);

                if (extensionMimeTypes.Count == 0)
                {
                    s_extensionMap.Value.Remove(extension);
                }
            }
        }
        s_mimeTypeMap.Value[mimeType] = new MimeData(encoding, extensionList);
    }

    /// <summary>
    ///   Retrieves the encoding of given <paramref name="mimeType"/>. If <paramref
    ///   name="mimeType"/> is not mapped, then an exception will be thrown.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <returns>The encoding of given <paramref name="mimeType"/>.</returns>
    /// <exception cref="ArgumentException"><paramref name="mimeType"/> is not mapped.</exception>
    public static MimeEncoding GetEncoding(string mimeType) => GetEncoding(mimeType, true);

    /// <summary>
    ///   Retrieves the encoding of given <paramref name="mimeType"/>. If <paramref
    ///   name="mimeType"/> is not mapped, then an exception will be thrown if <paramref
    ///   name="throwIfMissing"/> is true; otherwise, the default <see
    ///   cref="MimeEncoding.Base64"/> encoding is returned.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="throwIfMissing">
    ///   If true, throws an exception if <paramref name="mimeType"/> is not mapped.
    /// </param>
    /// <returns>The encoding of given <paramref name="mimeType"/>.</returns>
    /// <exception cref="ArgumentException">
    ///   <paramref name="mimeType"/> is not mapped and <paramref name="throwIfMissing"/> is true.
    /// </exception>
    public static MimeEncoding GetEncoding(string mimeType, bool throwIfMissing)
    {
        return (!TryGetEncoding(mimeType, out var encoding) && throwIfMissing)
            ? throw new ArgumentException($"Requested MIME type is not mapped: {mimeType}")
            : encoding;
    }

    /// <summary>
    ///   Retrieves the extension linked to <paramref name="mimeType"/>. If <paramref
    ///   name="mimeType"/> is not mapped, then an exception will be thrown.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <returns>The extension linked to <paramref name="mimeType"/>.</returns>
    /// <exception cref="ArgumentException"><paramref name="mimeType"/> is not mapped.</exception>
    public static string GetExtension(string mimeType) => GetExtension(mimeType, true);

    /// <summary>
    ///   Retrieves the extension linked to <paramref name="mimeType"/>. If <paramref
    ///   name="mimeType"/> is not mapped, then an exception will be thrown if <paramref
    ///   name="throwIfMissing"/> is true; otherwise, the default ".bin" extension is returned.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="throwIfMissing">
    ///   If true, throws an exception if <paramref name="mimeType"/> is not mapped.
    /// </param>
    /// <returns>The extension linked to <paramref name="mimeType"/>.</returns>
    /// <exception cref="ArgumentException">
    ///   <paramref name="mimeType"/> is not mapped and <paramref name="throwIfMissing"/> is true.
    /// </exception>
    public static string GetExtension(string mimeType, bool throwIfMissing)
    {
        return (!TryGetExtension(mimeType, out var extension) && throwIfMissing)
            ? throw new ArgumentException($"Requested MIME type is not mapped: {mimeType}")
            : extension;
    }

    /// <summary>
    ///   Retrieves the extensions linked to <paramref name="mimeType"/>. If <paramref
    ///   name="mimeType"/> is not mapped, then an empty list will be returned.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <returns>The extensions linked to <paramref name="mimeType"/>.</returns>
    public static IReadOnlyList<string> GetExtensions(string mimeType)
    {
        return s_mimeTypeMap.Value.TryGetValue(mimeType, out var data)
            ? data.Extensions
            : s_emptyReadOnlyStringCollection;
    }

    /// <summary>
    ///   Retrieves the extensions linked to <paramref name="mimeType"/> and strips the leading
    ///   dot character. If <paramref name="mimeType"/> is not mapped, then an empty list will
    ///   be returned.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <returns>The extensions linked to <paramref name="mimeType"/>.</returns>
    public static IReadOnlyList<string> GetExtensionsWithoutDot(string mimeType)
    {
        return GetExtensions(mimeType)
            .Select(e => e.Substring(1))
            .ToList()
            .AsReadOnly();
    }

    /// <summary>
    ///   Retrieves the extension linked to <paramref name="mimeType"/> and strips the leading
    ///   dot character. If <paramref name="mimeType"/> is not mapped, then an exception will be thrown.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <returns>
    ///   The extension linked to <paramref name="mimeType"/>, without the leading dot character.
    /// </returns>
    /// <exception cref="ArgumentException"><paramref name="mimeType"/> is not mapped.</exception>
    public static string GetExtensionWithoutDot(string mimeType) => GetExtensionWithoutDot(mimeType, true);

    /// <summary>
    ///   Retrieves the extension linked to <paramref name="mimeType"/> and strips the leading
    ///   dot character. If <paramref name="mimeType"/> is not mapped, then an exception will be
    ///   thrown if <paramref name="throwIfMissing"/> is true; otherwise, the default "bin"
    ///   extension is returned.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="throwIfMissing">
    ///   If true, throws an exception if <paramref name="mimeType"/> is not mapped.
    /// </param>
    /// <returns>
    ///   The extension linked to <paramref name="mimeType"/>, without the leading dot character.
    /// </returns>
    /// <exception cref="ArgumentException">
    ///   <paramref name="mimeType"/> is not mapped and <paramref name="throwIfMissing"/> is true.
    /// </exception>
    public static string GetExtensionWithoutDot(string mimeType, bool throwIfMissing) => GetExtension(mimeType, throwIfMissing).Substring(1);

    /// <summary>
    ///   Retrieves the MIME type linked to the extension of <paramref name="fileName"/>. If the
    ///   extension is not mapped, then an exception will be thrown.
    /// </summary>
    /// <param name="fileName">File name.</param>
    /// <returns>The MIME type linked to the extension of <paramref name="fileName"/>.</returns>
    /// <exception cref="ArgumentException">
    ///   The extension of <paramref name="fileName"/> is not mapped.
    /// </exception>
    /// <remarks>
    ///   If a file path is passed in instead of a simple file name, name will be automatically
    ///   extracted using <see cref="Path.GetFileName(string)"/> method.
    /// </remarks>
    public static string GetMimeType(string fileName) => GetMimeType(fileName, true);

    /// <summary>
    ///   Retrieves the MIME type linked to the extension of <paramref name="fileName"/>. If the
    ///   extension is not mapped, then an exception will be thrown if <paramref
    ///   name="throwIfMissing"/> is true; otherwise, the default "application/octet-stream"
    ///   MIME type is returned.
    /// </summary>
    /// <param name="fileName">File name.</param>
    /// <param name="throwIfMissing">
    ///   If true, throws an exception if the extension of <paramref name="fileName"/> is not mapped.
    /// </param>
    /// <returns>The MIME type linked to the extension of <paramref name="fileName"/>.</returns>
    /// <exception cref="ArgumentException">
    ///   The extension of <paramref name="fileName"/> is not mapped and <paramref
    ///   name="throwIfMissing"/> is true.
    /// </exception>
    /// <remarks>
    ///   If a file path is passed in instead of a simple file name, name will be automatically
    ///   extracted using <see cref="Path.GetFileName(string)"/> method.
    /// </remarks>
    public static string GetMimeType(string fileName, bool throwIfMissing)
    {
        var extension = Path.GetExtension(fileName);
        return (!TryGetMimeType(fileName, out var mimeType) && throwIfMissing)
            ? throw new ArgumentException($"Requested extension is not mapped: {extension}")
            : mimeType;
    }

    /// <summary>
    ///   Retrieves the MIME type related to specified <paramref name="fileStream"/>, by reading
    ///   its file signature. If the file signature is not mapped, then an exception will be thrown.
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <returns>The MIME type linked to the file signature of <paramref name="fileStream"/>.</returns>
    /// <exception cref="ArgumentException">
    ///   The file signature of <paramref name="fileStream"/> is not mapped.
    /// </exception>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    public static string GetMimeType(Stream fileStream) => GetMimeType(fileStream, true);

    /// <summary>
    ///   Retrieves the MIME type related to specified <paramref name="fileStream"/>, by reading
    ///   its file signature. If the file signature is not mapped, then an exception will be
    ///   thrown if <paramref name="throwIfMissing"/> is true; otherwise, the default
    ///   "application/octet-stream" MIME type is returned.
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <param name="throwIfMissing">
    ///   If true, throws an exception if the file signature of <paramref name="fileStream"/> is
    ///   not mapped.
    /// </param>
    /// <returns>The MIME type linked to the file signature of <paramref name="fileStream"/>.</returns>
    /// <exception cref="ArgumentException">
    ///   The file signature of <paramref name="fileStream"/> is not mapped and <paramref
    ///   name="throwIfMissing"/> is true.
    /// </exception>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    public static string GetMimeType(Stream fileStream, bool throwIfMissing)
    {
        return (!TryGetMimeType(fileStream, out var mimeType) && throwIfMissing)
            ? throw new ArgumentException("MIME type of specified stream is not mapped")
            : mimeType;
    }

    /// <summary>
    ///   Retrieves the MIME type related to specified <paramref name="fileStream"/>, by reading
    ///   its file signature, or the MIME type linked to the extension of <paramref
    ///   name="fileName"/>. If the signature or the extension are not mapped, then an exception
    ///   will be thrown.
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <param name="fileName">File name.</param>
    /// <returns>
    ///   The MIME type related to specified <paramref name="fileStream"/> or the MIME type
    ///   linked to the extension of <paramref name="fileName"/>.
    /// </returns>
    /// <exception cref="ArgumentException">
    ///   The file signature of <paramref name="fileStream"/> or the extension of <paramref
    ///   name="fileName"/> are not mapped.
    /// </exception>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    /// <remarks>
    ///   <para>File signature is evaluated first because it is usually more reliable.</para>
    ///   <para>
    ///     If a file path is passed in instead of a simple file name, name will be
    ///     automatically extracted using <see cref="Path.GetFileName(string)"/> method.
    ///   </para>
    /// </remarks>
    public static string GetMimeType(Stream fileStream, string fileName) => GetMimeType(fileStream, fileName, true);

    /// <summary>
    ///   Retrieves the MIME type related to specified <paramref name="fileStream"/>, by reading
    ///   its file signature, or the MIME type linked to the extension of <paramref
    ///   name="fileName"/>. If the signature or the extension are not mapped, then an exception
    ///   will be thrown if <paramref name="throwIfMissing"/> is true; otherwise, the default
    ///   "application/octet-stream" MIME type is returned.
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <param name="fileName">File name.</param>
    /// <param name="throwIfMissing">
    ///   If true, throws an exception if the file signature of <paramref name="fileStream"/> is
    ///   not mapped.
    /// </param>
    /// <returns>
    ///   The MIME type related to specified <paramref name="fileStream"/> or the MIME type
    ///   linked to the extension of <paramref name="fileName"/>.
    /// </returns>
    /// <exception cref="ArgumentException">
    ///   The file signature of <paramref name="fileStream"/> or the extension of <paramref
    ///   name="fileName"/> are not mapped and <paramref name="throwIfMissing"/> is true.
    /// </exception>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    /// <remarks>
    ///   <para>File signature is evaluated first because it is usually more reliable.</para>
    ///   <para>
    ///     If a file path is passed in instead of a simple file name, name will be
    ///     automatically extracted using <see cref="Path.GetFileName(string)"/> method.
    ///   </para>
    /// </remarks>
    public static string GetMimeType(Stream fileStream, string fileName, bool throwIfMissing)
    {
        return (!TryGetMimeType(fileStream, fileName, out var mimeType) && throwIfMissing)
            ? throw new ArgumentException("MIME type of specified stream and extension is not mapped")
            : mimeType;
    }

    /// <summary>
    ///   Retrieves the MIME types linked to the extension of <paramref name="fileName"/>. If
    ///   the extension is not mapped, then an empty collection will be returned.
    /// </summary>
    /// <param name="fileName">File name.</param>
    /// <returns>The MIME types linked to the extension of <paramref name="fileName"/>.</returns>
    /// <remarks>
    ///   If a file path is passed in instead of a simple file name, name will be automatically
    ///   extracted using <see cref="Path.GetFileName(string)"/> method.
    /// </remarks>
    public static IReadOnlyCollection<string> GetMimeTypes(string fileName)
    {
        var extension = Path.GetExtension(fileName);
        return s_extensionMap.Value.TryGetValue(extension, out var mimeTypes)
            ? mimeTypes.AsReadOnly()
            : s_emptyReadOnlyStringCollection;
    }

    /// <summary>
    ///   Retrieves the MIME types related to specified <paramref name="fileStream"/>, by
    ///   reading its file signature. If the file signature is not mapped, then an empty
    ///   collection will be returned.
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <returns>The MIME types linked to the file signature of <paramref name="fileStream"/>.</returns>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    public static IReadOnlyCollection<string> GetMimeTypes(Stream fileStream)
    {
        var mimeTypes = InspectFileSignature(fileStream);
        return mimeTypes.Count == 0 ? s_emptyReadOnlyStringCollection : new ReadOnlyCollection<string>(mimeTypes);
    }

    /// <summary>
    ///   Retrieves the MIME types related to specified <paramref name="fileStream"/>, by
    ///   reading its file signature, or the MIME types linked to the extension of <paramref
    ///   name="fileName"/>. If the signature or the extension are not mapped, then an empty
    ///   collection will be returned.
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <param name="fileName">File name.</param>
    /// <returns>
    ///   The MIME types related to specified <paramref name="fileStream"/> and the MIME types
    ///   linked to the extension of <paramref name="fileName"/>.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    /// <remarks>
    ///   <para>File signature is evaluated first because it is usually more reliable.</para>
    ///   <para>
    ///     If a file path is passed in instead of a simple file name, name will be
    ///     automatically extracted using <see cref="Path.GetFileName(string)"/> method.
    ///   </para>
    /// </remarks>
    public static IReadOnlyCollection<string> GetMimeTypes(Stream fileStream, string fileName)
    {
        var mimeTypesFromFileStream = InspectFileSignature(fileStream);
        var mimeTypesFromFileName = GetMimeTypes(fileName);

        if (mimeTypesFromFileStream.Count == 0)
        {
            return (mimeTypesFromFileName.Count == 0) ? s_emptyReadOnlyStringCollection : mimeTypesFromFileName;
        }
        if (mimeTypesFromFileName.Count == 0)
        {
            return new ReadOnlyCollection<string>(mimeTypesFromFileStream);
        }

        var mimeTypes = mimeTypesFromFileStream.ToList();
        mimeTypes.AddRange(mimeTypesFromFileName.Where(mt => !mimeTypes.Contains(mt)));

        return mimeTypes;
    }

    /// <summary>
    ///   Retrieves the encoding of given <paramref name="mimeType"/>. If the MIME type is not
    ///   mapped, then this method returns false and <paramref name="encoding"/> is set to <see cref="MimeEncoding.Base64"/>.
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="encoding">Matching extension.</param>
    /// <returns>
    ///   True if the encoding of given <paramref name="mimeType"/> exists, false otherwise.
    /// </returns>
    public static bool TryGetEncoding(string mimeType, out MimeEncoding encoding)
    {
        if (s_mimeTypeMap.Value.TryGetValue(mimeType, out var data))
        {
            encoding = data.Encoding;
            return true;
        }
        encoding = DefaultEncoding;
        return false;
    }

    /// <summary>
    ///   Retrieves the extension linked to <paramref name="mimeType"/>. If the MIME type is not
    ///   mapped, then this method returns false and <paramref name="extension"/> is set to ".bin".
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="extension">Matching extension.</param>
    /// <returns>
    ///   True if the extension linked to <paramref name="mimeType"/> exists, false otherwise.
    /// </returns>
    public static bool TryGetExtension(string mimeType, out string extension)
    {
        if (s_mimeTypeMap.Value.TryGetValue(mimeType, out var data))
        {
            extension = data.Extensions[0];
            return true;
        }
        extension = DefaultExtension;
        return false;
    }

    /// <summary>
    ///   Retrieves the extension linked to <paramref name="mimeType"/> and strips the leading
    ///   dot character. If the MIME type is not mapped, then this method returns false and
    ///   <paramref name="extension"/> is set to ".bin".
    /// </summary>
    /// <param name="mimeType">MIME type.</param>
    /// <param name="extension">Matching extension, without the leading dot character.</param>
    /// <returns>
    ///   True if the extension linked to <paramref name="mimeType"/> exists, false otherwise.
    /// </returns>
    public static bool TryGetExtensionWithoutDot(string mimeType, out string extension)
    {
        var matched = TryGetExtension(mimeType, out extension);
        extension = extension.Substring(1);
        return matched;
    }

    /// <summary>
    ///   Retrieves the MIME type linked to the extension of <paramref name="fileName"/>. If the
    ///   extension is not mapped, then this method returns false and <paramref
    ///   name="mimeType"/> is set to "application/octet-stream".
    /// </summary>
    /// <param name="fileName">File name.</param>
    /// <param name="mimeType">Matching MIME type.</param>
    /// <returns>
    ///   True if the MIME type linked to the extension of <paramref name="fileName"/> exists,
    ///   false otherwise.
    /// </returns>
    /// <remarks>
    ///   If a file path is passed in instead of a simple file name, name will be automatically
    ///   extracted using <see cref="Path.GetFileName(string)"/> method.
    /// </remarks>
    public static bool TryGetMimeType(string fileName, out string mimeType)
    {
        var extension = Path.GetExtension(fileName);
        if (s_extensionMap.Value.TryGetValue(extension, out var mimeTypes))
        {
            mimeType = mimeTypes[0];
            return true;
        }
        mimeType = DefaultMimeType;
        return false;
    }

    /// <summary>
    ///   Retrieves the MIME type related to specified <paramref name="fileStream"/>, by reading
    ///   its file signature, or the MIME type linked to the extension of <paramref
    ///   name="fileName"/>. If the signature or the extension are not mapped, then this method
    ///   returns false and <paramref name="mimeType"/> is set to "application/octet-stream".
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <param name="fileName">File name.</param>
    /// <param name="mimeType">Matching MIME type.</param>
    /// <returns>
    ///   True if the MIME type related to specified <paramref name="fileStream"/> can be
    ///   identified from its signature or the MIME type linked to the extension of <paramref
    ///   name="fileName"/> exists, false otherwise.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    /// <remarks>
    ///   <para>File signature is evaluated first because it is usually more reliable.</para>
    ///   <para>
    ///     If a file path is passed in instead of a simple file name, name will be
    ///     automatically extracted using <see cref="Path.GetFileName(string)"/> method.
    ///   </para>
    /// </remarks>
    public static bool TryGetMimeType(Stream fileStream, string fileName, out string mimeType)
    {
        // File signature is checked first: if a non-generic MIME type is deduced from that,
        // then it is returned to the caller. If more than one MIME type matched, or if MIME
        // type is deemed as too generic, then we try to find further information from file name.
        var mimeTypesFromFileStream = InspectFileSignature(fileStream);
        var mimeTypeFromFileStream = mimeTypesFromFileStream.FirstOrDefault();
        if (mimeTypesFromFileStream.Count == 1 && mimeTypeFromFileStream != null && !s_tooGenericMimeTypes.Contains(mimeTypeFromFileStream))
        {
            mimeType = mimeTypeFromFileStream;
            return true;
        }

        // If an intersection between MIME types obtained from file stream and the ones obtained
        // from file name exist, then it is returned to the caller.
        var mimeTypesFromFileName = GetMimeTypes(fileName);
        var mimeTypesFromIntersection = mimeTypesFromFileStream.Intersect(mimeTypesFromFileName).ToList();
        if (mimeTypesFromIntersection.Count > 0)
        {
            mimeType = mimeTypesFromIntersection[0];
            return true;
        }

        // If file name check yields a result, then it is returned to the caller.
        if (mimeTypesFromFileName.Count > 0)
        {
            mimeType = mimeTypesFromFileName.First();
            return true;
        }

        // If no MIME type can be deduced from file name, then MIME type obtained from file
        // signature, if exists, is returned to the caller.
        if (mimeTypeFromFileStream != default)
        {
            mimeType = mimeTypeFromFileStream;
            return true;
        }

        // No MIME type was detected, neither from file stream nor from file name.
        mimeType = DefaultMimeType;
        return false;
    }

    /// <summary>
    ///   Retrieves the MIME type related to specified <paramref name="fileStream"/>, by reading
    ///   its file signature. If the file signature is not mapped, then this method returns
    ///   false and <paramref name="mimeType"/> is set to "application/octet-stream".
    /// </summary>
    /// <param name="fileStream">File stream.</param>
    /// <param name="mimeType">Matching MIME type.</param>
    /// <returns>
    ///   True if the MIME type related to specified <paramref name="fileStream"/> can be
    ///   identified from its signature, false otherwise.
    /// </returns>
    /// <exception cref="NotSupportedException">
    ///   <paramref name="fileStream"/> is not seekable.
    /// </exception>
    public static bool TryGetMimeType(Stream fileStream, out string mimeType)
    {
        var mimeTypes = InspectFileSignature(fileStream);
        if (mimeTypes.Count > 0)
        {
            mimeType = mimeTypes[0];
            return true;
        }
        mimeType = DefaultMimeType;
        return false;
    }

    #region File signature inspection

    /// <summary>
    ///   <para>Used to identify OLECF files. Each stream name is probed again file root storage.</para>
    ///   <para>Source data: https://github.com/neilharvey/FileSignatures</para>
    /// </summary>
    private static readonly Lazy<Dictionary<string, IList<string>>> s_olecfStreamProbes = new(() => new()
    {
        ["__properties_version1.0"] = s_extensionMap.Value[Extensions.MSG],
        ["Book"] = s_extensionMap.Value[Extensions.XLS],
        ["PowerPoint Document"] = s_extensionMap.Value[Extensions.PPT],
        ["VisioDocument"] = s_extensionMap.Value[Extensions.VSD],
        ["WordDocument"] = s_extensionMap.Value[Extensions.DOC],
        ["Workbook"] = s_extensionMap.Value[Extensions.XLS],
    });

    /// <summary>
    ///   How long should be the buffer used to probe file signatures.
    /// </summary>
    private static readonly Lazy<int> s_probeBufferLength = new(() => s_fileSignatures!.Value.Max(s => s.Signature.Length + s.MaxOffset));

    private static IList<string> InspectFileSignature(Stream fileStream)
    {
        if (!fileStream.CanSeek)
        {
            throw new NotSupportedException("File stream needs to be seekable");
        }

        if (fileStream.Length == 0L)
        {
            // Empty file stream, not match can be found.
            return Array.Empty<string>();
        }

        // Buffer length needs to take into account signature length and required offset.
        var buffer = new byte[s_probeBufferLength.Value];
        try
        {
            var byteCount = fileStream.Read(buffer, 0, buffer.Length);

            foreach (var signature in s_fileSignatures.Value)
            {
                if (TryMatchFileSignature(fileStream, buffer, byteCount, signature, out var mimeTypes))
                {
                    return mimeTypes;
                }
            }

            // If we got here, it means that no signature has matched. Therefore, we try to
            // understand if current file is textual. In that case, the "text/plain" MIME type
            // is returned.
            if (MightBeTextFile(fileStream))
            {
                return new[] { TEXT.PLAIN };
            }

            // No match has been found.
            return Array.Empty<string>();
        }
        finally
        {
            // Reset stream position.
            fileStream.Position = 0L;
        }
    }

    // https://stackoverflow.com/questions/910873/how-can-i-determine-if-a-file-is-binary-or-text-in-c
    private static bool MightBeTextFile(Stream fileStream)
    {
        // Restore stream position. Stream supports seeking, otherwise file signature operation,
        // which happens before this method, would have failed.
        fileStream.Position = 0L;

        var buffer = new byte[512];
        var byteCount = fileStream.Read(buffer, 0, buffer.Length);

        for (var i = 1; i < byteCount; i++)
        {
            // Two consecutive NULL characters are usually contained in binary files.
            if (buffer[i] == 0x00 && buffer[i - 1] == 0x00)
            {
                return false;
            }
        }

        // No NULL character has been found, it might be a text file.
        return true;
    }

    private static bool TryMatchFileSignature(
        Stream fileStream, byte[] buffer, int byteCount,
        FileSignature signature, out IList<string> mimeTypes)
    {
        for (var offset = signature.MinOffset; offset <= signature.MaxOffset; offset++)
        {
            if (byteCount < (signature.Signature.Length + offset))
            {
                // Signature is longer than file contents, we should skip it.
                mimeTypes = Array.Empty<string>();
                return false;
            }

            var index = offset;
            foreach (var b in signature.Signature)
            {
                if (!b.Contains(buffer[index]))
                {
                    // Current byte does not match file signature, we need to try another one.
                    continue;
                }
                index++;
            }

            if ((index - offset) != signature.Signature.Length)
            {
                continue;
            }

            // A signature match has been found, so related MIME types are evaluated.
            mimeTypes = signature.MimeTypes(fileStream);
            if (mimeTypes.Count > 0)
            {
                // A valid match has been found.
                return true;
            }
        }

        // Signature did not match.
        mimeTypes = Array.Empty<string>();
        return false;
    }

    private static IList<string> InspectOlecfFileSignature(Stream fileStream)
    {
        // Restore stream position. Stream supports seeking, otherwise file signature operation,
        // which happens before this method, would have failed.
        fileStream.Position = 0L;

        try
        {
            using var compoundFile = new CompoundFile(fileStream, CFSUpdateMode.ReadOnly, CFSConfiguration.LeaveOpen);
            var rootStorage = compoundFile.RootStorage;

            return s_olecfStreamProbes.Value
                .Where(sp => rootStorage.TryGetStream(sp.Key, out var _))
                .Select(sp => sp.Value)
                .SingleOrDefault() ?? Array.Empty<string>();
        }
        catch
        {
            // Nothing to do.
        }

        // No match has been found.
        return Array.Empty<string>();
    }

    private static IList<string> InspectZipFileSignature(Stream fileStream)
    {
        try
        {
            using var zipArchive = new ZipArchive(fileStream, ZipArchiveMode.Read, leaveOpen: true);

            // Office Open XML (OOXML) archives might contain a specific file, vbaProject.bin,
            // which implies the presence of user-defined macros.
            // Therefore, OOXML detection logic should both look for XML files, 
            // used to detect Excel/PowerPoint/Word file family, and for that BIN file.
            List<string> ooxmlMimeTypesWithoutMacros = [];
            List<string> ooxmlMimeTypesWithMacros = [];
            bool? ooxmlFileWithMacros = null;

            foreach (var entry in zipArchive.Entries)
            {
                switch (entry.FullName.ToLowerInvariant())
                {
                    // OpenDocument (ODT, ODS, ODP, ...).
                    case "mimetype":
                        using (var entryStream = entry.Open())
                        using (var streamReader = new StreamReader(entryStream))
                        {
                            var rawMimeType = streamReader.ReadToEnd();
                            if (MediaTypeHeaderValue.TryParse(rawMimeType, out var parsedMimeType) && parsedMimeType.MediaType != null)
                            {
                                return [parsedMimeType.MediaType];
                            }
                            throw new InvalidDataException($"OpenDocument MIME type \"{rawMimeType}\" could not be parsed");
                        }

                    // Office Open XML.
                    case "xl/workbook.xml":
                        ooxmlMimeTypesWithoutMacros = s_extensionMap.Value[Extensions.XLSX].Union(s_extensionMap.Value[Extensions.XLTX]).ToList();
                        ooxmlMimeTypesWithMacros = s_extensionMap.Value[Extensions.XLSM];
                        break;

                    case "ppt/presentation.xml":
                        ooxmlMimeTypesWithoutMacros = s_extensionMap.Value[Extensions.PPTX].Union(s_extensionMap.Value[Extensions.POTX]).ToList();
                        ooxmlMimeTypesWithMacros = s_extensionMap.Value[Extensions.PPTM];
                        break;

                    case "word/document.xml":
                    case "word/document2.xml":
                        ooxmlMimeTypesWithoutMacros = s_extensionMap.Value[Extensions.DOCX].Union(s_extensionMap.Value[Extensions.DOTX]).ToList();
                        ooxmlMimeTypesWithMacros = s_extensionMap.Value[Extensions.DOCM];
                        break;

                    case "fixeddocseq.fdseq":
                        return s_extensionMap.Value[Extensions.XPS];

                    case "xl/vbaproject.bin":
                    case "ppt/vbaproject.bin":
                    case "word/vbaproject.bin":
                        ooxmlFileWithMacros = true;
                        break;
                }

                if (ooxmlFileWithMacros.HasValue && ooxmlMimeTypesWithoutMacros.Count > 0 && ooxmlMimeTypesWithMacros.Count > 0)
                {
                    return ooxmlFileWithMacros.Value ? ooxmlMimeTypesWithMacros : ooxmlMimeTypesWithoutMacros;
                }
            }

            if (ooxmlMimeTypesWithoutMacros.Count > 0 && ooxmlMimeTypesWithMacros.Count > 0)
            {
                return ooxmlFileWithMacros == true ? ooxmlMimeTypesWithMacros : ooxmlMimeTypesWithoutMacros;
            }
        }
        catch (InvalidDataException)
        {
            // Corrupted ZIP file.
            return Array.Empty<string>();
        }

        // It seems to be a simple ZIP file.
        return s_extensionMap.Value[Extensions.ZIP];
    }

    private struct FileSignature
    {
        private int _minOffset;

        public Func<Stream, IList<string>> MimeTypes { get; set; }

        public int MinOffset
        {
            get => _minOffset;
            set
            {
                _minOffset = value;
                if (MaxOffset < _minOffset)
                {
                    MaxOffset = _minOffset;
                }
            }
        }

        public int MaxOffset { get; set; }

        public ByteRange[] Signature { get; set; }
    }

    private struct ByteRange
    {
        public static ByteRange Any { get; } = new(byte.MinValue, byte.MaxValue);

        public static ByteRange Number { get; } = new(0x30, 0x39);

        public byte MinValue { get; }

        public byte MaxValue { get; }

        public ByteRange(byte value)
        {
            MinValue = value;
            MaxValue = value;
        }

        public ByteRange(byte minValue, byte maxValue)
        {
            MinValue = minValue;
            MaxValue = maxValue;
        }

        public bool Contains(byte value) => MinValue <= value && value <= MaxValue;

        public override string ToString() => $"{MinValue:X2}, {MaxValue:X2}";

        public static implicit operator ByteRange(int value) => new((byte)value);
    }

    #endregion
}
