﻿// Copyright (c) PommaLabs Team and Contributors <hello@pommalabs.xyz>. All rights reserved.
//
// Licensed under the MIT License. See LICENSE file in the project root for full license information.

using System;
using System.Linq;

namespace PommaLabs.MimeTypes.Generator;

/// <summary>
///   Custom filters for DotLiquid.
/// </summary>
internal static class CustomFilters
{
    private static readonly char[] s_splitChars = new[] { '-', '+', '.', '@' };

    public static string Sharpify(string w)
    {
        var c = w[0];

        w = string.Join("_", w
            .Split(s_splitChars, StringSplitOptions.RemoveEmptyEntries)
            .Select(x => x.ToUpperInvariant()));

        return (char.IsDigit(c) || s_splitChars.Contains(c)) ? $"_{w}" : w;
    }
}
